<?php

namespace Omnipay\WorldPay;

use Omnipay\Common\AbstractGateway;

/**
 * WorldPay XML Direct Gateway
 *
 * @link http://www.worldpay.com/support/bg/xml/kb/submittingtransactionsdirect/dxml.html
 */
class DirectGateway extends AbstractGateway
{
    public function getName()
    {
        return 'WorldPay XML Direct';
    }

    public function getDefaultParameters()
    {
        return array(
            'installationId' => '',
            'merchantCode'   => '',
            'secretWord'     => '',
            'testMode'       => false
        );
    }

    public function getInstallationId()
    {
        return $this->getParameter('installationId');
    }

    public function setInstallationId($value)
    {
        return $this->setParameter('installationId', $value);
    }

    public function getMerchantCode()
    {
        return $this->getParameter('merchantCode');
    }

    public function setMerchantCode($value)
    {
        return $this->setParameter('merchantCode', $value);
    }

    public function getSecretWord()
    {
        return $this->getParameter('secretWord');
    }

    public function setSecretWord($value)
    {
        return $this->setParameter('secretWord', $value);
    }

    public function purchase(array $parameters = array())
    {
            return $this->createRequest('\Omnipay\WorldPay\Message\DirectPurchaseRequest', $parameters);
    }

    public function completePurchase(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\WorldPay\Message\DirectCompletePurchaseRequest', $parameters);
    }
}
