<?php

namespace Omnipay\WorldPay\Message;

use Omnipay\Common\Exception\InvalidResponseException;

/**
 * WorldPay XML Direct Complete Purchase Request
 */
class DirectCompletePurchaseRequest extends DirectPurchaseRequest
{
    public function getData()
    {
        $md = $this->httpRequest->request->get('MD');
        $paRes = $this->httpRequest->request->get('PaRes');
        if (empty($md) || empty($paRes)) {
            throw new InvalidResponseException;
        }

        $data = $this->getBaseData();

        $info3d = $data->submit->order->paymentDetails->addChild('info3DSecure');
        $info3d->paResponse = $paRes;

        $data->submit->order->echoData = $md;

        return $data;
    }
}
