<?php

/*
 * This file is part of the Omnipay package.
 *
 * (c) Adrian Macneil <adrian@adrianmacneil.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Omnipay\WorldPay\Message;

use DOMImplementation;
use SimpleXMLElement;

use Guzzle\Plugin\Cookie\CookiePlugin;
use Guzzle\Plugin\Cookie\CookieJar\FileCookieJar;

use Omnipay\Common\Message\AbstractRequest as BaseAbstractRequest;

/**
 * Worldpay XML Direct Abstract Request
 */
abstract class AbstractRequest extends BaseAbstractRequest
{
    protected $liveEndpoint = 'https://%s:%s@secure.worldpay.com/jsp/merchant/xml/paymentService.jsp';
    protected $testEndpoint = 'https://%s:%s@secure-test.worldpay.com/jsp/merchant/xml/paymentService.jsp';
    protected $serviceVersion = '1.4';
    protected $cardBrandMap = array(
        'visa'        => 'VISA-SSL',
        'mastercard'  => 'ECMC-SSL',
        'discover'    => 'DISCOVER-SSL',
        'amex'        => 'AMEX-SSL',
        'diners_club' => 'DINERS-SSL',
        'jcb'         => 'JCB-SSL',
        'switch'      => 'SWITCH-SSL',
        'solo'        => 'SOLO_GB-SSL',
        'dankort'     => 'DANKORT-SSL',
        'maestro'     => 'MAESTRO-SSL',
        'laser'       => 'LASER-SSL'
    );

    public function getInstallationId()
    {
        return $this->getParameter('installationId');
    }

    public function setInstallationId($value)
    {
        return $this->setParameter('installationId', $value);
    }

    public function getMerchantCode()
    {
        return $this->getParameter('merchantCode');
    }

    public function setMerchantCode($value)
    {
        return $this->setParameter('merchantCode', $value);
    }

    public function getSecretWord()
    {
        return $this->getParameter('secretWord');
    }

    public function setSecretWord($value)
    {
        return $this->setParameter('secretWord', $value);
    }

    public function getBaseData()
    {
        $this->validate('amount', 'card');
        $this->getCard()->validate();

        $data = new SimpleXMLElement('<paymentService/>');
        $data->addAttribute('version', $this->serviceVersion);

        if ($this->getMerchantCode()) {
            $data->addAttribute('merchantCode', $this->getMerchantCode());
        }

        $data->addChild('submit');
        $order = $data->submit->addChild('order');
        $order->addAttribute('orderCode', $this->getTransactionId());
        $order->addAttribute('installationId', $this->getInstallationId());
        $order->addChild('description', $this->getDescription());

        $amount = $order->addChild('amount');

        $amount->addAttribute('value', $this->getAmountInteger());
        $amount->addAttribute('currencyCode', $this->getCurrency());
        $amount->addAttribute('exponent', 2);

        $payment = $order->addChild('paymentDetails');

        $card = $payment->addChild($this->getCardBrand());
        $card->addChild('cardNumber', $this->getCard()->getNumber());

        $card->addChild('expiryDate');
        
        $expiryDate = $card->expiryDate->addChild('date');
        $expiryDate->addAttribute('month', $this->getCard()->getExpiryDate('m'));
        $expiryDate->addAttribute('year', $this->getCard()->getExpiryDate('Y'));

        $card->addChild('cardHolderName', $this->getCard()->getName());
        $card->addChild('cvc', $this->getCard()->getCvv());

        $card->addChild('cardAddress');
        $address = $card->cardAddress->addChild('address');

        $address->addChild('firstName', $this->getCard()->getFirstName());
        $address->addChild('lastName', $this->getCard()->getLastName());
        $address->addChild('address1', $this->getCard()->getAddress1());
        $address->addChild('address2', $this->getCard()->getAddress2());
        $address->addChild('postalCode', $this->getCard()->getPostcode());
        $address->addChild('city', $this->getCard()->getCity());
        $address->addChild('countryCode', $this->getCard()->getCountry());
        $address->addChild('telephoneNumber', $this->getCard()->getPhone());

        $session = $payment->addChild('session');

        if ($this->getClientIp()) {
            $session->addAttribute('shopperIPAddress', $this->getClientIp());
        }

        $session->addAttribute('id', $this->getTransactionId());

        $shopper = $order->addChild('shopper');
        $shopper->addChild('shopperEmailAddress', $this->getCard()->getEmail());
        
        $browser = $shopper->addChild('browser');
        $browser->addChild('acceptHeader', $_SERVER['HTTP_ACCEPT']);
        $browser->addChild('userAgentHeader', $_SERVER['HTTP_USER_AGENT']);

        $order->addChild('shippingAddress');
        $saddress = $order->shippingAddress->addChild('address');

        $saddress->addChild('firstName', $this->getCard()->getShippingFirstName());
        $saddress->addChild('lastName', $this->getCard()->getShippingLastName());
        $saddress->addChild('address1', $this->getCard()->getShippingAddress1());
        $saddress->addChild('address2', $this->getCard()->getShippingAddress2());
        $saddress->addChild('postalCode', $this->getCard()->getShippingPostcode());
        $saddress->addChild('city', $this->getCard()->getShippingCity());
        $saddress->addChild('countryCode', $this->getCard()->getShippingCountry());
        $saddress->addChild('telephoneNumber', $this->getCard()->getShippingPhone());

        return $data;
    }

    public function sendData($data)
    {
        $data = $this->getData();

        $implementation = new DOMImplementation();

        $dtd = $implementation->createDocumentType('paymentService',
        '-//WorldPay/DTD WorldPay PaymentService v1//EN',
        'http://dtd.worldpay.com/paymentService_v1.dtd');

        $document = $implementation->createDocument('', '', $dtd);

        $sxe = $document->importNode(dom_import_simplexml($data), true);
        $document->appendChild($sxe);

        // post to WorldPay
        $headers = array('Content-Type' => 'text/xml; charset=utf-8');

        $dir = function_exists('sys_get_temp_dir') ? sys_get_temp_dir() : '/tmp';
        $cookieFile = $dir . '/wpd_' . $this->getTransactionId();

        if ( !file_exists($cookieFile) ) {
            if ( @!is_dir( $dir ) || !is_writable( $dir ) || file_put_contents($cookieFile, null) === FALSE ) {
                throw new Exception('Could not create storage cookie file');
            }
        }

        $cookiePlugin = new CookiePlugin(new FileCookieJar($cookieFile));
        $this->httpClient->addSubscriber($cookiePlugin);

        $httpResponse = $this->httpClient->post($this->getEndpoint(), $headers, $document->saveXML())->send();

        foreach ($cookiePlugin->getCookieJar()->all() as $cookie) {
            $cookie->setExpires(time() + 300);
        }

        return $this->response = new DirectPurchaseResponse($this, $httpResponse->getBody());
    }

    public function getEndpoint()
    {
        $endpoint = $this->getTestMode() ? $this->testEndpoint : $this->liveEndpoint;
        return sprintf($endpoint, $this->getMerchantCode(), $this->getSecretWord());
    }

    protected function getCardBrand()
    {
        $brand = $this->getCard()->getBrand();

        if (isset($this->cardBrandMap[$brand])) {
            return $this->cardBrandMap[$brand];
        }

        return $brand;
    }
}
