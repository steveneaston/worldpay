<?php

namespace Omnipay\WorldPay\Message;

use DOMDocument;

use Omnipay\Common\Message\AbstractResponse;
use Omnipay\Common\Message\RedirectResponseInterface;

use Omnipay\Common\Message\RequestInterface;
use Omnipay\Common\Exception\InvalidResponseException;

/**
 * WorldPay Purchase Response
 */
class DirectPurchaseResponse extends AbstractResponse implements RedirectResponseInterface
{
    public function __construct(RequestInterface $request, $data)
    {
        $this->request = $request;

        $responseDom = new DOMDocument;
        $responseDom->loadXML($data);
        $this->data = simplexml_import_dom($responseDom->documentElement->firstChild->firstChild);
    }

    public function isSuccessful()
    {
        if (isset($this->data->payment->lastEvent)) {
            return 'AUTHORISED' === (string) $this->data->payment->lastEvent;
        }

        return false;
    }

    public function isRedirect()
    {
        return isset($this->data->requestInfo);
    }

    public function getTransactionReference()
    {
        return $this->getRequest()->getTransactionId();
    }

    public function getMessage()
    {
        if (isset($this->data->payment->lastEvent)) {
            return (string) $this->data->payment->lastEvent;
        }

        return (string) $this->data;
    }

    public function getRedirectUrl()
    {
        if ($this->isRedirect()) {
            return (string) $this->data->requestInfo->request3DSecure->issuerURL;
        }
    }

    public function getRedirectMethod()
    {
        return 'POST';
    }

    public function getRedirectData()
    {
        return $redirectData = array(
            'PaReq' => (string) $this->data->requestInfo->request3DSecure->paRequest,
            'TermUrl' => $this->getRequest()->getReturnUrl(),
            'MD' => (string) $this->data->echoData,
        );
    }

}
