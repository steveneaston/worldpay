<?php

namespace Omnipay\WorldPay\Message;

/**
 * WorldPay XML Direct Purchase Request
 */
class DirectPurchaseRequest extends AbstractRequest
{
    public function getCookieName()
    {
        return $this->getParameter('cookieName');
    }

    public function setCookieName($value)
    {
        return $this->setParameter('cookieName', $value);
    }

    public function getData()
    {
        return $this->getBaseData();
    }
}
